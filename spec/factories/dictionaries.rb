# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :dictionary do
    term  "#{Faker::Company.suffix} #{Faker::Company.name}"
    definition "Suffix: #{Faker::Company.suffix} | Phrase: #{Faker::Company.catch_phrase} | Bs: #{Faker::Company.bs} | Number: #{Faker::Company.duns_number}"
  end
end
