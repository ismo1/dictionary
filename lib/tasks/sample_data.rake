namespace :db do
  desc "Drop and create the current database and adding somes example of users & projetcs"
  task populate: :environment do
    require 'populator'
    require 'faker'

    puts "Startitng rails populate !!!!"
    puts "Create Ditionaries words.."
    make_words
    puts "End of tasks db is rady !!!"
  end
end

def make_words
  Dictionary.destroy_all
  Dictionary.create(term: "PC", definition: "personnal computer")
  Dictionary.create(term: "Ismael", definition: "Préson de personnne")
  Dictionary.create(term: "Moussa Seybou", definition: "Le nom de Ismael")

  10.times do |n|
    term = Faker::Company.name
    definition = "CEO: #{Faker::Name.name} | Suffix: #{Faker::Company.suffix} | Bs: #{Faker::Company.bs} | Number: #{Faker::Company.duns_number}"
    Dictionary.create(term: term, definition: definition)
  end

  10.times do |n|
    term = Faker::Name.name
    definition = "Address: #{Faker::Address.street_address}, #{Faker::Address.city}, #{Faker::Address.postcode}, #{Faker::Address.country}"
    Dictionary.create(term: term, definition: definition)
  end

   10.times do |n|
    term =Faker::Lorem.word
    definition = Faker::Lorem.sentence(3)
    Dictionary.create(term: term, definition: definition)
  end
end