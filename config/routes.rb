Webservices::Application.routes.draw do
  resources :dictionaries
  # resources :dictionaries, :collection => { :recent => :get }, :member => { :modify => :put }
  #resources :dictionaries,  :collection => { :recent => :get }, :member     => { :modify => :put }
  # get    "/search",  to: 'dictionaries#search',       as: 'search'
  get    "/autocomplete",  to: 'dictionaries#autocomplete',       as: 'autocomplete'
  # match 'search', to: 'dictionaries#search', via: [:get, :post]

  root :to => "dictionaries#index"
end
