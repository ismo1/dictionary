class DictionariesController < ApplicationController
  before_filter :authorize
  before_action :set_dictionary, only: [:show, :edit, :update, :destroy]

  respond_to :html, :xml, :json, :js

  # GET /dictionaries
  def index
    if params[:query].present?
      @dictionaries = Dictionary.search_query(params[:query], params[:page])
    else
      @dictionaries = Dictionary.order(:term).paginate(page: params[:page],:per_page=> 10)
    end
    respond_with @dictionaries

    # respond_to do |format|
    #   format.html
    #   format.json { render json: @dictionaries }
    #   format.xml  { render xml: @dictionaries }
    # end
  end

  def autocomplete
    render json: Dictionary.search_autocomplete(params[:query])
    # render json: Dictionary.search(params[:query], autocomplete: true, fields: [{term: :text_start}], limit: 15).map(&:term)
  end

  # GET /dictionaries/1
  def show
  end

  # GET /dictionaries/new
  def new
    respond_with @dictionary = Dictionary.new
  end

  # GET /dictionaries/1/edit
  def edit
  end

  # POST /dictionaries
  def create
    @dictionary = Dictionary.new(dictionary_params)

    flash[:notice] = 'Dictionary was successfully created.' if @dictionary.save
    respond_with @dictionary
  end

  # PATCH/PUT /dictionaries/1
  def update
    if @dictionary.update(dictionary_params)
      respond_with @dictionary, notice: 'Dictionary was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /dictionaries/1
  def destroy
    @dictionary.destroy
    redirect_to dictionaries_url, notice: 'Dictionary was successfully destroyed.'
  end

  def recent
    render text: 'recent'
  end

  def modify
    render text: 'modify'
  end

  protected
    def authorize
      authenticate_or_request_with_http_basic do |username, password|
        username == 'admin' && password == 'secret'
      end
    end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dictionary
      @dictionary = Dictionary.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def dictionary_params
      params.require(:dictionary).permit(:term, :definition)
    end
end
