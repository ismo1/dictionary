class Dictionary < ActiveRecord::Base
  # searchkick
  searchkick autocomplete: ['term'], text_start: ['term'], suggest: ["term"]


  def search_data
    as_json only: [:term, :definition, :created_at]
    {
      term: term,
      definition: definition,
      created_at: created_at
    }
  end

  def self.search_query(param, page)
    Dictionary.search "%#{param}%", suggest: true, misspellings: {distance: 2}, page: page, per_page: 10, suggest: true
  end

  def self.search_autocomplete(param)
    Dictionary.search(param, fields: [{term: :text_start}], limit: 15).map(&:term)
  end
  # Dictionary.reindex

  # def self.search(param)
  #   # Dictionary.where(:first, :conditions => ['lower(term) LIKE ?', "%#{param.downcase}%"])
  #   # Dictionary.where('lower(term) LIKE ?', "%#{param.downcase}%").order(:term)
  #   # Dictionary.search("%#{param.downcase}%", where: {in_stock: true}, limit: 10, offset: 50)
  #   Dictionary.search("%#{param}%")
  # end
end
