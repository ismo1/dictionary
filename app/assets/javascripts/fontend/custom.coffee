# $.ajax(url: "/dictionaries").done (html) ->
#   console.log(html)
#   # $("#query").append html

paintIt = (element, backgroundColor, textColor) ->
  element.style.backgroundColor = backgroundColor
  if textColor?
    element.style.color = textColor

$(document).on "page:change", ->
  $("a[data-background-color]").click ->
    backgroundColor = $(this).data("background-color")
    textColor = $(this).data("text-color")
    paintIt(this, backgroundColor, textColor)

$ ->
  $("#query").typeahead
  name: "query"
  remote: "/autocomplete?query=%QUERY"
